import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MapService} from '../../services/map.service';
import {ParentChildService} from '../../services/parent-child.service';
import {CapitalCity} from '../../models/capitalCity';
import * as Capitals from '../../../assets/json/capitalCities.json';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {

  defaultViewCoords = [7.242149, 48.841583];
  centerForClickedArea;
  target = false;
  cities: CapitalCity[];
  round = 0;
  currentKmLeft = 1500;
  currentDistance: number;
  successCount = 0;
  currentCity = '';
  endGame = false;
  highestScore: number;
  citiesCenter = [];

  constructor(private mapService: MapService, private parentChild: ParentChildService) { }

  ngOnInit() {
    this.mapService.createMap('map', this.defaultViewCoords);
    this.cities = Capitals.capitalCities;
    setTimeout(() => {this.currentCity = this.cities[this.round].capitalCity});
  }

  ngAfterViewInit() {

    if (!localStorage.getItem('highestScore')) {
      localStorage.setItem('highestScore', '0');
    }

    const parentChild = this.parentChild;
    const mapService = this.mapService;


    function getCoordinatesOnClick (map) {
      map.addEventListener('click', function (evt) {
          const coord = mapService.getCoordinates(evt);
          parentChild.changeSubscribtion(coord);
          return coord;
        });
    }
    getCoordinatesOnClick(this.mapService.getMap());

    this.parentChild.currentSubscribtion.subscribe(val => {
      this.centerForClickedArea = val;
      if (this.centerForClickedArea && this.target) {
        this.gameLogic();
      }
    });
  }

  enablePlaceMarker() {
    this.target = true;
    document.getElementById('map').style.cursor = 'pointer';
  }

  gameLogic() {
    this.target = false;
    document.getElementById('map').style.cursor = 'default';
    this.currentCity = this.cities[this.round].capitalCity;
    this.citiesCenter = [+this.cities[this.round].long, +this.cities[this.round].lat];
    this.mapService.createMarkers(this.centerForClickedArea, this.citiesCenter);
    this.currentDistance = this.gameCalculation(this.round, this.centerForClickedArea);
    if (this.currentDistance <= 50) {
      this.successCount++;
    }
    this.currentKmLeft = this.currentKmLeft - this.currentDistance;
    if (this.currentKmLeft <= 0) {
      this.endGame = true;
      if (+localStorage.getItem('highestScore') >= this.successCount) {
        this.highestScore = +localStorage.getItem('highestScore');
      } else {
        this.highestScore = this.successCount;
        localStorage.setItem('highestScore', this.successCount.toString());
      }
    }
    this.round++;
    if (this.round >= this.cities.length) {
      this.round = 0;
    }
    this.currentCity = this.cities[this.round].capitalCity;
  }

  gameCalculation(round: number, clickedCenter: any) {
    const latLon1 = [+this.cities[round].lat, +this.cities[round].long];
    const latLon2 = [clickedCenter[1], clickedCenter[0]];
    return this.mapService.calculateDistance(latLon1, latLon2);
  }

  startAgain() {
    this.endGame = false;
    this.target = false;
    document.getElementById('map').style.cursor = 'default';
    this.round = 0;
    this.currentCity = this.cities[this.round].capitalCity;
    this.currentKmLeft = 1500;
    this.successCount = 0;
    this.currentDistance = 0;
    this.mapService.removeMarkerLayer();
  }




}
