import { Injectable } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import OlVectorSource from 'ol/source/Vector';
import Icon from 'ol/style/Icon';
import OlXYZ from 'ol/source/XYZ';
import * as olProj from 'ol/proj';
import {fromLonLat, getPointResolution, transform, Projection} from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import GeoJSON from 'ol/format/GeoJSON';
import OlFeature from 'ol/Feature';
import OlPoint from 'ol/geom/Point';
import {Style} from 'ol/style.js';
import {getDistance} from 'ol/sphere';
import {ZoomToExtent, defaults as defaultControls} from 'ol/control';


@Injectable({
  providedIn: 'root'
})
export class MapService {

  map: Map;
  source: OlXYZ;
  geoJSONURL = '../assets/json/european-union-countries.geojson';
  tileSourceURL = 'https://tiles.wmflabs.org/osm-no-labels/{z}/{x}/{y}.png';
  vectorLayer: VectorLayer;

  clickedPinStyle = new Style({
    image: new Icon({
      src: '../assets/icons/pin.png',
    }),
  });

  cityPinStyle = new Style({
    image: new Icon({
      src: '../assets/icons/city_pin.png',
    }),
  });

  constructor() {
  }

  // method allowing to call for openstreetmap

  createMap(target, coords) {

    this.source = new OlXYZ({
      url: this.tileSourceURL
    });

    this.map = new Map({
      controls: defaultControls(),
      target: target,
      layers: [
        new TileLayer({
          source: this.source
        })
      ],
      view: new View({
        center: olProj.fromLonLat(coords),
        zoom: 4
      })
    });

    this.addGeoJSON(this.geoJSONURL);
  }

  // simple method to access map
  getMap() {
    return this.map;
  }

  // add additional vector layer to render more clear EU countries borders (taken from geojson file)

  addGeoJSON(url) {
    const geojsonSource = new OlVectorSource({
      url: url,
      format: new GeoJSON()
    });

    const geojsonLayer = new VectorLayer({
      source: geojsonSource
    });

    this.map.addLayer(geojsonLayer);
  }

  // method allows to remove layer with markers

  removeMarkerLayer() {
    this.map.removeLayer(this.vectorLayer);
  }

  //method that creates and placing markers on the map

  createMarkers(clickedCenter, actualCenter) {
    if (this.vectorLayer) {
      this.map.removeLayer(this.vectorLayer);
    }
    const clicked_marker = new OlFeature({
      geometry: new OlPoint(fromLonLat(clickedCenter))
    });

   clicked_marker.setStyle(this.clickedPinStyle);

    const actual_marker = new OlFeature({
      geometry: new OlPoint(fromLonLat(actualCenter))
    });

    actual_marker.setStyle(this.cityPinStyle);

    const vectorSource = new OlVectorSource({
      features: [clicked_marker, actual_marker]
    });

    this.vectorLayer = new VectorLayer({
      source: vectorSource
    });

    this.map.addLayer(this.vectorLayer);

  }

  getCoordinates(evt) {
    return olProj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
  }

  calculateDistance(latlng1, latlng2) {
    return Math.round((getDistance(latlng1, latlng2)) / 1000);
  }
}



