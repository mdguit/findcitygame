import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParentChildService {

  private subscribe = new BehaviorSubject<[number]>(null);
  currentSubscribtion = this.subscribe.asObservable();

  constructor() { }

  changeSubscribtion(message: [number]) {
    this.subscribe.next(message);
  }
}
