# FindCityGame

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

# Answers to the questions

1. Tested on Samsung Galaxy A50. Browsers (both desktop and mobile) - Google Chrome, Firefox and Samsung Browser.
2. Highest score stored in localStorage and in the end of each game being compared with the current result, if current result is higher then the on stored inside localStorage the value there will be overwritten.
3. I would add information pop-up with the rules of the game.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
